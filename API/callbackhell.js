const getData = ((url, callback) => {
    let request = new XMLHttpRequest();
    request.addEventListener ('readystatechange', () => {
        if (request.readyState === 4 && request.status === 200) {
            const data = request.responseText;
            callback (undefined, data);
        }else if (request.readyState === 4) {
            callback ("can't find the data", undefined);
        }
    });
    request.open ('GET', url);
    request.send ();
})

getData ('https://jsonplaceholder.typicode.com/posts', (err, data) => {
    console.log (data);
    getData ('https://jsonplaceholder.typicode.com/comments', (err, data) => {
        console.log (data);
    });
    getData ('https://jsonplaceholder.typicode.com/albums', (err, data) => {
        console.log (data);
    })
})