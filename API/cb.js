const data = ((val) => {
    return new Promise ((resolve, reject) => {
        return val % 2 === 0 ? resolve ('even') : reject ('odd');
    })
})

data(5).then ((res) => {
    console.log (res);
}).catch ((e) => {
    console.log (e + ' catch block');
})