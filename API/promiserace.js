let promise1 = new Promise ((resolve, reject) => {
    const request = new XMLHttpRequest();
    request.addEventListener ('readystatechange', () => {
        if (request.readyState === 4 && request.status === 200) {
            const data = JSON.parse (request.responseText);
            resolve (data);
        }else if (request.readyState === 4) {
            reject ('No data found');
        }
    });
    request.open ('GET', 'https://jsonplaceholder.typicode.com/users');
    request.send ();
})

let promise2 = new Promise ((resolve, reject) => {
    const request = new XMLHttpRequest();
    request.addEventListener ('readystatechange', () => {
        if (request.readyState === 4 && request.status === 200) {
            const data = JSON.parse (request.responseText);
            resolve (data);
        }else if (request.readyState === 4) {
            reject ('No data found');
        }
    });
    request.open ('GET', 'https://jsonplaceholder.typicode.com/users');
    request.send ();
})

let promise3 = new Promise ((resolve, reject) => {
    const request = new XMLHttpRequest();
    request.addEventListener ('readystatechange', () => {
        if (request.readyState === 4 && request.status === 200) {
            const data = JSON.parse (request.responseText);
            resolve (data);
        }else if (request.readyState === 4) {
            reject ('No data found');
        }
    });
    request.open ('GET', 'https://jsonplaceholder.typicode.com/users');
    request.send ();
})

Promise.race ([promise1, promise2, promise3]).then ((data) => {
    console.log (data);
}).catch ((err) => {
    console.log (err);
})