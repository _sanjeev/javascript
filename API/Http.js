const getTodo = (resources, callBack) => {
    const request = new XMLHttpRequest();

    request.addEventListener('readystatechange', () => {
        // console.log (request, request.readyState);
        if (request.readyState === 4 && request.status == 200) {
            // console.log(request.responseText);
            const data = JSON.parse (request.responseText);
            callBack (undefined, data);
        } else if (request.readyState === 4) {
            // console.log('Could not find request');
            callBack ("Coundn't find data", undefined);
        }
    });

    // request.open('GET', 'todo.json');
    request.open ('GET', resources)
    request.send();
}

// getTodo('todo.json', (err, data) => {
//     console.log ('callback Fired');
//     if (err) {
//         console.log (err);
//     } else {
//         console.log (data);
//     }
// });

// Promise example

const getSomething = () => {
    return new Promise((resolve, reject) => {
        // fetch something
        // resolve ('some data')
        reject ('some error');
    });
}

// getSomething ().then ((data) => {
//     console.log (data);
// }, (err) => {
//     console.log (err);
// })

getSomething ()
.then ((data) => {
    console.log (data);
})
.catch((err) => {
    console.log (err);
});
