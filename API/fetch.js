import fetch from 'node-fetch';
const apiUrl = 'https://jsonpplaceholder.typicode.com/todos/1';

// fetch (apiUrl).then ((response) => {
//     return response.json();
// }).then ((data) => {
//     console.log (data);
// }).catch ((err) => {
//     console.log (err);
// })

const fetching = (async (cb) => {
    try {
        const response = await fetch(apiUrl);
        const data = await response.json();
        console.log(data);
    } catch (e) {
        console.log ('hello');
    }
})

fetching('hello');