 const getTodo = ((url, callback) => {
     let request = new XMLHttpRequest();
     request.addEventListener ('readystatechange', () => {
         if (request.readyState === 4 && request.status === 200) {
             const data = JSON.parse (request.responseText);
             callback (undefined, data);
         } else if (request.readyState === 4){
             callback ("can't find the data", undefined);
         }
     });
     request.open ('GET', url);
     request.send ();
 })
const url = 'https://jsonplaceholder.typicode.com/posts';
 getTodo (url, (err, data) => {
     console.log ('callback Fired');
     if (err) {
         console.log (err);
     } else {
        //  console.log (data[0].title);
        for (let index = 0; index < data.length; index++) {
            console.log (data[index].title);
        }
     }
 })