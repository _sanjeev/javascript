const getData = (url) => {
    return new Promise ((resolve, reject) => {
        const request = new XMLHttpRequest();
        request.addEventListener ('readystatechange', () => {
            if (request.readyState === 4 && request.status === 200) {
                const data = JSON.parse (request.responseText);
                resolve (data);
            }else if (request.readyState === 4) {
                reject ("can't find the data");
            }
        });
        request.open ('GET', url);
        request.send ();
    });
}

// getData ('https://jsonplaceholder.typicode.com/albums').then ((data) => {
//     console.log (data);
//     return getData ('https://jsonplaceholder.typicode.com/posts');
// }).then ((data) => {
//     console.log (data);
//     return getData ('https://jsonplaceholder.typicode.com/comments');
// }).then ((data) => {
//     console.log (data);
// }).catch ((err) => {
//     console.log (err);
// })

getData ('https://jsonplaceholder.typicode.com/comments').then ((data) => {
    console.log (data);
    return getData ('https://jsonplaceholder.typicode.com/posts');
}).then ((data) => {
    console.log (data);
}).catch ((err) => {
    console.log (err);
})