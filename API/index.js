// Create an object of request.
const request = new XMLHttpRequest ();
const url = 'https://jsonplaceholder.typicode.com/posts';

request.addEventListener ('readystatechange', () => {
    // console.log (request, request.readyState);
    if (request.readyState === 4 && request.status === 200) {
        console.log (request.responseText);
    }else if (request.readyState === 4) {
        console.log ('Not Find the data');
    }
})

//send a request
request.open ('GET', url);
request.send ();