let promise1 = new Promise ((resolve, reject) => {
    const request = new XMLHttpRequest();
    request.addEventListener ('readystatechange', () => {
        if (request.readyState === 4 && request.status === 200) {
            const data = JSON.parse (request.responseText);
            resolve (data);
        }else if (request.readyState === 4) {
            reject ('No data found');
        }
    });
    request.open ('GET', "https://jsonplaceholder.typicode.com/posts");
    request.send ();
})

let promise2 = new Promise ((resolve, reject) => {
    const request = new XMLHttpRequest();
    request.addEventListener ('readystatechange', () => {
        if (request.readyState === 4 && request.status === 200) {
            const data = JSON.parse (request.responseText);
            resolve (data);
        }else if (request.readyState === 4) {
            reject ('No data found');
        }
    });
    request.open ('GET', "https://jsonplaceholder.typicode.com/posts");
    request.send ();
})

// Promise.all ([promise1, promise2]).then ((data) => {
//     console.log (data);
// }).catch ((err) => {
//     console.log (err);
// })
const getData = (async () => {
    let response = await fetch ('https://jsonplaceholder.typicode.com/users');
    let data = await response.json ();
    return data;
})

// getData ().then ((data) => {
//     console.log (data);
// }).catch ((err) => {
//     console.log (err);
// })

Promise.all ([promise1, promise2]).then ((data) => {
    console.log (data);
}).catch ((err) => {
    console.log (err);
})


