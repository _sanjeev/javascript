const getTodo = ((url, callback) => {
    let request = new XMLHttpRequest();
    request.addEventListener ("readystatechange", () => {
        if (request.readyState === 4 && request.status === 200) {
            callback (undefined, request.responseText);
        } else if (request.readyState === 4) {
            callback ("can't find the data", undefined);
        }
    })
    request.open ('GET', url);
    request.send ();
});

console.log (1);
console.log (2);

const url = 'https://jsonplaceholder.typicode.com/posts';

getTodo (url, (err, data) => {
    console.log ('callback Fired');
    if (data) {
        console.log (data);
    } else {
        console.log (err);
    }
})

console.log (3);
console.log (4);