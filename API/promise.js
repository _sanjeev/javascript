const getData = ((url) => {
    return new Promise ((resolve, reject) => {
        const request = new XMLHttpRequest ();
        request.addEventListener ('readystatechange', () => {
            if (request.readyState === 4 && request.status === 200) {
                const data = JSON.parse (request.responseText);
                resolve (data);
            }else if (request.readyState === 4){
                reject ('cannot find data');
            }
        });
        request.open ('GET', url);
        request.send ();
    })
})
const url = 'https://jsonplaceholder.typicode.com/albums';
getData (url).then ((data) => {
    console.log (data);
}).catch ((err) => {
    console.log (err);
})