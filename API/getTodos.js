const getTodo = (url => {
    const request = new XMLHttpRequest();
    request.addEventListener ('readystatechange', () => {
        if (request.readyState === 4 && request.status === 200) {
            console.log (request.responseText);
            // return request.responseText;
        } else if (request.readyState === 4) {
            console.log ('cannot find the data');
        }
    })
    request.open ('GET', url);
    request.send ();
});

const url = 'https://jsonplaceholder.typicode.com/posts';
const res = getTodo (url);
console.log (res);