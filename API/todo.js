const fun1 = () => {
    console.log('hi');
}
const fun2 = () => {
    return new Promise((resolve, reject) => {
        resolve('hello');
    })
}
const fun3 = () => {
    console.log('Bye');
}
const fun4 = (cb) => {
    console.log('Dear');
}
fun1();
fun2().then((resolve) => console.log(resolve));
fun3();
fun4();
