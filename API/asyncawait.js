
const getData = (async () => {
    const response = await fetch ('https://jsonplaceholder.typicode.com/comments');
    if (response.status !== 200) {
        throw new Error ('cannot fetch the data');
    }
    const data = await response.json();
    return data;
})

getData ().then ((allData) => {
    let output = '';
    allData.forEach ((data) => {
        output += `<table style = 'border : 2px solid red'><tr><th  style = 'border : 2px solid red'>${data.postId}</th><th  style = 'border : 2px solid red'>${data.id}</th><th style = 'border : 2px solid red'>${data.name}</th><th style = 'border : 2px solid red'>${data.email}</th><th  style = 'border : 2px solid red'>${data.body}</th></tr></table>`;
    });
    document.body.innerHTML = output;
}).catch (err => {
    console.log (err);
});
