const getData = ((url, callback) => {
    let request = new XMLHttpRequest();
    request.addEventListener ('readystatechange', () => {
        if (request.readyState === 4 && request.status === 200) {
            const data = JSON.parse (request.responseText);
            callback (undefined, data);
        } else if (request.readyState === 4) {
            callback ("can't find the data", undefined);
        }
    });
    request.open ('GET', url);
    request.send ();
});

const url = './todo.json';
getData (url, (err, data) => {
    if (data) {
        console.log (data[0]);
    } else {
        console.log (err);
    }
})