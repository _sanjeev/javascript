import fetch  from "node-fetch";

const data = (async(cb) => {
    try {
        const response = await fetch ('https://jsonplaceholder.typicode.com/photos');
        const data = await response.json();
        cb (data);
    }catch (e) {
        console.log (e);
    }
})

const cb = (data) => {
    console.log (data[0].url)
}

data (cb);
