import fetch from "node-fetch";
const endPoint = 'http://openlibrary.org/api/volumes/brief/isbn/9780525440987.json';

const fetchData = ((url) => {
    return new Promise ((resolve, reject) => {
        fetch (url).then ((data) => {
            return data.json();
        }).then ((data) => {
            resolve (data);
        }).catch ((err) => {
            reject ('Not find the data');
        })
    })
})

fetchData (endPoint).then ((data) => {
    let obj = data.records;
    const out = Object.entries (obj).filter ((key) => {
        const res = Object.entries (key[1]).reduce ((acc, val) => {
            console.log(val);
        }, {})
    })
}).catch ((err) => {
    console.log(err);
})