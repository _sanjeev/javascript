'use strict';
const demo = () => {
    console.log (this);
}


let obj = {
    prep: 12,
    print: function () {
        console.log (this);
    }
};

demo.call(obj);

// console.log (obj.print())
// Used in strict mode

 