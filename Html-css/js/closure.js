let i = 10;
//    global scope => i = 10 11 12
//       outer =>  j = 20 21
//       inner => k = 30

function outer() {
    let j = 20;                       
    const inner = () => {
        let k = 30;
        console.log(i, j, k);
        i++;
        j++;
        k++;
    }
    return inner;
}

const inner = outer();
inner();
inner();