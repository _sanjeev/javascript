let student = [
    {name : 'sahil', age : 18, address : {city : 'saran', pincode : 111111}},
    {name : 'Tom', age : 28, address : {city : 'USA', pincode : 111171}},
    {name : 'Tim', age : 18, address : {city : 'CHINA', pincode : 111111}},
    {name : 'John', age : 28, address : {city : 'CHINA', pincode : 161111}},
    {name : 'Bob', age : 8, address : {city : 'USA', pincode : 115611}},
    {name : 'Raju', age : 58, address : {city : 'DUBAI', pincode : 311111}},
    {name : 'Tony', age : 38, address : {city : 'FRANCE', pincode : 161111}},
];

// for (let i = 0; i < student.length; i++) {
//     // console.log (student[i].age);
//     console.log (student[i].address.city)
// }

for (let i in student) {
    console.log (i);
}