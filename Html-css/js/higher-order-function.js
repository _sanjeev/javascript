const companies = [
    { name: "Company one", category: "Finance", start: 1981, end: 2003 },
    { name: "Company Two", category: "Retail", start: 1992, end: 2008 },
    { name: "Company Three", category: "Auto", start: 1999, end: 2007 },
    { name: "Company Four", category: "Retail", start: 1989, end: 2010 },
    { name: "Company Five", category: "Technology", start: 2009, end: 2014 },
    { name: "Company Six", category: "Finance", start: 1987, end: 2010 },
    { name: "Company Seven", category: "Auto", start: 1986, end: 1996 },
    { name: "Company Eight", category: "Technology", start: 2011, end: 2016 },
    { name: "Company Nine", category: "Retail", start: 1981, end: 1989 },
];

const age = [33, 12, 20, 16, 5, 54, 21, 44, 61, 13, 15, 45, 25, 64, 32];

// for (let index = 0; index < companies.length; index++) {
//     console.log (companies[index]);
// }

//forEach

// const callback = (company, index, companies) => {
//     console.log (company.name);
// }
// companies.forEach (callback);

// const callback = (val, index, age) => {
//     console.log (val);
//     console.log (index);
//     console.log (age);
// }

// age.forEach (callback);

// Filter

// const callback = (value, index) => {
//     if (index > 8) {
//         return true;
//     }
// }
// const candrink = age.filter (callback);

// console.log (candrink);


// Filtering the Object

// const callback = (company, index, companies) => {
//     if (company.category === 'Retail') {
//         return true;
//     }
// }

// const returnValue = companies.filter (callback);
// console.log (returnValue);

// ---------------------------------------
// Predefined Filter Function
// const callback = (value) => {
//     if (value > 21) {
//         return true;
//     }
// }

// function filter (age, callback) {
//     let output = [];
//     for (let index = 0; index < age.length; index++) {
//         if (callback (age[index])) {
//             output.push (age[index]);
//         }
//     }
//     return output;
// }

// let actualResult = filter (age, callback);
// console.log (actualResult);

// ------------------------------------------

// Map

// const callback = (value, index, arr) => {
//     return value + 10;
// }

// const result = age.map (callback);
// console.log (result);


// const callback = (company, index, companies) => {
//     return company.name = 'Ford ' + company.name;
// }

// const result = companies.map (callback);
// console.log (result);

// ---------------------------------------------

// sort

// const callback = (company1, company2) => {
//     if (company1.start > company2.start) {
//         return 1;
//     } else {
//         return -1;
//     }
// }


// const sortedCompanies = companies.sort (callback);
// console.log (sortedCompanies);

// const callback = (a, b) => {
//     return a - b;
// }
// const returnValue = age.sort (callback);
// console.log (returnValue);

// const result = age.sort ((a, b) => {return a - b;});
// console.log (result);

// ------------------------------------------

// const callback = (value, age) => {
//     return value + age;
// }
// const res = age.reduce (callback, 0);
// console.log (res);


// const callback = (value, accumulator) => {
//     return value + accumulator;
// }

// function reduce (age, callback) {
//     let res = 0;
//     for (let index = 0; index < age.length; index++) {
//         res = res + callback (age[index], 0);
//     }
//     return res;
// }
// console.log (reduce (age, callback));
const callback = (total, company) => {
    return total + (company.end - company.start);
}
const totalYears = companies.reduce (callback, 0);
console.log (totalYears);