let a = {
    firstName : 'Ram',
    lastName : 'Shyam',
    age : 18,
    country : 'India',
    favMovie : ['one', 'two', 'three'],
    living : {
        'city' : 'saran',
        'country' : 'India',
    },
    salary : function () {
        return 25000;
    },
    name : function (){
        return this.firstName + ' ' + this.lastName;
    },
    color : function (name) {
        return name;
    }
};
console.log (a);
console.log (a.favMovie);
console.log (a.favMovie[1]);
console.log (a.name());
console.log (a.color ('white'));
console.log (a.living.city);
console.log (a.living['city']);
console.log (a.living.country);
console.log (a.living['country']);



