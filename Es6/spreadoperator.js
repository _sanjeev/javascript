const arr1 = [1,2,3];
const arr2 = [4,5,6];

const res = arr1.concat (arr2);
console.log (res);
const out = [...arr1, ...arr2];
console.log (out);

let data = {
    fname: 'Tom',
    age: 45,
    gender: "m",
}

let data2 = {
    lon: 3.43,
    lat: 2.34,
}

const anotherData = {...data};
// console.log (data);
// console.log (anotherData);

const newData = {...data, ...data2};
console.log (newData);
