class Abstraction {
    set (x, y) {
        this.a = x;
        this.b = y;
    }
    display () {
        console.log ('a = ' + this.a);
        console.log ('b = ' + this.b);
    }
}

const obj = new Abstraction ();
obj.set (10, 20);
obj.display();