const person = {
    name: 'sahil',
    walk() {
        console.log (this);
    }
}

// person.walk();
// const walking = person.walk.bind (person);
// walking();
// const point = walk;
// point();
// walk();

const data = {
    name: 'sahil',
    test: function() {
        // console.log (this);
        setTimeout (() => {
            console.log (this);
        })
    }
}

data.test();