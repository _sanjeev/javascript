class Person {
    constructor (name, email, phone) {
        this.name = name;
        this.email = email;
        this.phone = phone;
    }
    display () {
        console.log ('hello');
    }
}
class Teacher extends Person{
    constructor (name, email, phone, subject) {
        super (name, email, phone);
        this.subject = subject;
    }
    display () {
        super.display();
        console.log ('hii');
    }
}
let obj = new Teacher ('sahil', 'abc', '111111', 'Data structures');
console.log (obj);
obj.display();
// obj.data();