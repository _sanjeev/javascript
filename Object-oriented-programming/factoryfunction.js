const factoryFunction = (name, email, phone) => {
    return {
        name,
        email,
        phone
    }
}

const obj = factoryFunction ('sahil', 'abc@gmail.com', '111111');

obj.address = 'Bangalore';
delete obj.address;
console.log (obj);