export class User {
    constructor (name, email, phone) {
        this.name = name;
        this.email = email;
        this.phone = phone;
    }

}

export class Admin extends User{
    constructor (name, email, phone, paymentDetails) {
        super (name, email, phone);
        this.paymentDetails = paymentDetails;
    }
    display () {
        console.log ('Hello');
    }
}

// module.exports = {Admin, User};